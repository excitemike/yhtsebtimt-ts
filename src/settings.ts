export const TITLE = "You Have to Save Everyone But There Isn't Much Time!";
export const WIDTH = 450;
export const HEIGHT = 250;
export const FPS = 30;
export const GRAVITY = 1000;
export const MAX_FALL_SPEED = 600;
export const LETHAL_FALL_SPEED = MAX_FALL_SPEED;
export const CAMERA_TIGHTNESS = 0.125;
export const ENEMY_WALK_SPEED = 20;
export const ENEMY_HOP_SPEED = 20;
export const STOMPED_YSCALE = 0.3;
export const PLATFORMS = [
    new Phaser.Geom.Rectangle(50,240,800,10),    // big floor
    new Phaser.Geom.Rectangle(450, 220, 50, 10), // floating platform
    new Phaser.Geom.Rectangle(300, 220, 50, 20), // wall to jump over
    new Phaser.Geom.Rectangle(700, 160, 100, 20) // high platform
];
export const COLOR_BRIGHTEST = 0xFFFFCC;
export const COLOR_BRIGHT = 0xD5D5B3;
export const COLOR_MED = 0xABAB9A;
export const COLOR_DARK = 0x666652;
export const COLOR_DARKEST = 0x303000;
export const PLATFORM_COLOR = COLOR_DARK;

export const PLAYER_WALK_SPEED = 110;
export const PLAYER_JUMP_SPEED = 300;
export const PLAYER_ENEMY_BOUNCE_SPEED = 450;

export const DEATH_HOP_SPEED = 200;
export const DEATH_ROTATION = 720; // degrees / second
export const BLOOD_PARTICLES_PER_DEATH = 50.0;
export const TIME_LIMIT = 10.0;
export const WAIT_FOR_ANIMATION_DELAY = 1.0;
export const EVENT_DEATH = "EVENT_DEATH";

export const MUSIC_LIST = [
    "https://www.meyermike.com/games/minild9/angrybass.mp3",
    "https://www.meyermike.com/games/minild9/annoyed303synth.mp3",
    "https://www.meyermike.com/games/minild9/darkthingscrawlupinyourskullsynth.mp3",
    "https://www.meyermike.com/games/minild9/determined303synth.mp3",
    "https://www.meyermike.com/games/minild9/drumloop1.mp3",
    "https://www.meyermike.com/games/minild9/drumloop2.mp3",
    "https://www.meyermike.com/games/minild9/drumloop3.mp3",
    "https://www.meyermike.com/games/minild9/drumloop4.mp3",
    "https://www.meyermike.com/games/minild9/drumloop5.mp3",
    "https://www.meyermike.com/games/minild9/drumloop6.mp3",
    "https://www.meyermike.com/games/minild9/drumloop7.mp3",
    "https://www.meyermike.com/games/minild9/drumloop8.mp3",
    "https://www.meyermike.com/games/minild9/drumloop9.mp3",
    "https://www.meyermike.com/games/minild9/drumloop10.mp3",
    "https://www.meyermike.com/games/minild9/grungybass.mp3",
    "https://www.meyermike.com/games/minild9/lofimelody.mp3",
    "https://www.meyermike.com/games/minild9/lofipad.mp3",
    "https://www.meyermike.com/games/minild9/ogglysynth.mp3",
    "https://www.meyermike.com/games/minild9/synthguitar.mp3",
    "https://www.meyermike.com/games/minild9/tonalbass.mp3",
    "https://www.meyermike.com/games/minild9/trancechordsynth.mp3",
    "https://www.meyermike.com/games/minild9/twinklysynth.mp3"];
export const MAX_VOLUME = 0.5;
