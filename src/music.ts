import * as cfg from "./settings";

let playlist:string[] = [];
let music_src:string = null;
let playing:boolean = false
let audioElem:HTMLAudioElement = null;
let initted:boolean = false;
let interacted:boolean = false;
let volume:number = cfg.MAX_VOLUME;

/// set the options for playRandom()
export function setPlaylist(urls:string[]):void{
    playlist = urls;
}

export function playRandom():void{
    if (playlist.length > 0) {
        setMusic(playlist[(Math.random() * playlist.length)|0]);
    }
}

/// start some music looping
export function setMusic(url: string) {
    if (!initted) { init(); }
    if ((music_src != null) && (music_src != url)) {
        killMusic();
    }
    music_src = url;
    audioElem = audioElem || new Audio(url);
    audioElem.src = url;
    audioElem.loop = true;
    audioElem.volume = volume;
    audioElem.addEventListener('playing', () => playing = true);
    audioElem.addEventListener('pause', () => playing = false);
    tryPlay();
}

// [0,1]
export function setVolume(v:number):void {
    volume = v;
    audioElem && (audioElem.volume = v);
}
export function getVolume():number {
    return volume;
}

export function toggleMusic() {
    playing ? audioElem.pause() : audioElem.play();
}

/// clean up whatever music is already happening
function killMusic() {
    // TODO: it didn't seem like it always stops
    if (audioElem) {
        audioElem.pause();
        playing = false;
    }
}

/// react to interaction from the user (which gives permission to play music)
function interactionHandler() {
    if (!interacted) {
        document.removeEventListener('click', interactionHandler);
        document.removeEventListener('touchend', interactionHandler);
        interacted = true;
        if (music_src != null) {
            tryPlay();
        }
    }
}

/// initial setup
function init() {
    if (!initted) {
        document.addEventListener('click', interactionHandler);
        document.addEventListener('touchend', interactionHandler);
        initted = true;
    }
}

/// attempt to start the music
function tryPlay() {
    if (!playing && (audioElem != null)) {
        audioElem
            .play()
            .catch((e) => {
                audioElem.addEventListener('canplaythrough', () => {
                    tryPlay();
                });
            });
    }
}
