import * as Phaser from 'phaser';
import { PreloaderScene } from './scenes/preloader-scene';
import { MainScene } from './scenes/main-scene';
import { IntroScene, TryAgainScene, YouWinScene } from './scenes/other-scenes';
import * as cfg from './settings';

window.addEventListener('load', () => {
    const game = new Phaser.Game({
        width: cfg.WIDTH,
        height:cfg.HEIGHT,
        type: Phaser.AUTO,
        scene: [ PreloaderScene, IntroScene, MainScene, TryAgainScene, YouWinScene ],
        title: cfg.TITLE,
        parent: 'game',
        input: { keyboard: true },
        banner: false,
        fps: { target: cfg.FPS, forceSetTimeOut: true },
        disableContextMenu: false,
        transparent: false,
        render: { pixelArt: true, antialias: false }
    });
});