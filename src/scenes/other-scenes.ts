import { BaseScene } from './base-scene';
import * as Music from "../music";

class BaseWordsScene extends BaseScene {
    private nextSceneName:string;
    createFromAnim(animKey:string,nextSceneName:string):void{
      this.nextSceneName = nextSceneName;
      const x = 0.5 * this.cameras.main.width;
      const y = 0.5 * this.cameras.main.height;
      Music.playRandom();
      const img = this.add.sprite(x, y, "words", 0).anims.play(animKey);
      this.scheduleOnce(500, () => {
          this.input.keyboard.addListener(Phaser.Input.Keyboard.Events.ANY_KEY_DOWN, () => this.nextScene());
      });
    }
    createFromFrames(start:number,end:number,ms_per_frame:number,nextSceneName:string):void {
        this.nextSceneName = nextSceneName;
        const x = 0.5 * this.cameras.main.width;
        const y = 0.5 * this.cameras.main.height;
        Music.playRandom();
        const img = this.add.sprite(x, y, "words", start);
        this.scheduleOnce(ms_per_frame, () => {
            img.setFrame(start+1);
            this.input.keyboard.addListener(Phaser.Input.Keyboard.Events.ANY_KEY_DOWN, () => this.nextScene());
        });
        for(let i=start+2;i<end;++i){
            this.scheduleOnce(ms_per_frame * (i-start), () => img.setFrame(i));
        }
    }
    nextScene():void {
        this.cleanupSchedule();
        this.input.keyboard.removeAllListeners(Phaser.Input.Keyboard.Events.ANY_KEY_DOWN);
        this.scene.stop();
        this.scene.run(this.nextSceneName);
    }
}

export class IntroScene extends BaseWordsScene {
  constructor() {
    super({key: "IntroScene"});
  }
  create():void{
    this.createFromAnim('intro', 'MainScene');
  }
}
export class TryAgainScene extends BaseWordsScene {
    constructor() {
      super({key: "TryAgainScene"});
    }
    create():void{
      this.createFromAnim('tryAgain', 'MainScene');
    }
}
export class YouWinScene extends BaseWordsScene {
    constructor() {
      super({key: "YouWinScene"});
    }
    create():void{
      this.createFromAnim('youWin', 'MainScene');
    }
}

