export class BaseScene extends Phaser.Scene {
    private cancelThese: number[];
    constructor(config:string | Phaser.Types.Scenes.SettingsConfig) {
        super(config);
        this.cancelThese = [];
    }

    scheduleOnce(delay_in_ms: number, f: () => void): void {
      this.cancelThese.push(window.setTimeout(f, delay_in_ms));
    }
  
    cleanupSchedule(): void {
      for (let n of this.cancelThese) {
        window.clearTimeout(n);
      }
      this.cancelThese.length = 0;
    }
}