import * as Music from "../music";
import * as cfg from "../settings";

export class PreloaderScene extends Phaser.Scene {
  private loadingBar: Phaser.GameObjects.Graphics;
  private progressBar: Phaser.GameObjects.Graphics;

  constructor() {
    super({ key: "PreloaderScene" });
  }

  preload(): void {
    this.cameras.main.setBackgroundColor(0x303000);
    this.createLoadingGraphics();

    // fill loading bar as we load
    this.load.on(
      "progress",
      (value: number) => {
        this.progressBar.clear();
        this.progressBar.fillStyle(0xffffff, 1);
        this.progressBar.fillRect(
          this.cameras.main.width / 4,
          this.cameras.main.height / 2 - 16,
          (this.cameras.main.width / 2) * value,
          16
        );
      },
      this
    );

    // delete on complete
    this.load.on(
      "complete",
      () => {
        this.progressBar.destroy();
        this.loadingBar.destroy();
        this.createAnimations();
        Music.setPlaylist(cfg.MUSIC_LIST);
      },
      this
    );

    this.load.pack("preload", "./assets/pack.json", "preload");
  }

  create(): void {
    this.cameras.main.backgroundColor = new Phaser.Display.Color(
      48,
      48,
      0,
      255
    );

    // start game scenes
    this.scene.stop("PreloaderScene");
    this.scene.run("IntroScene");
  }

  createLoadingGraphics(): void {
    this.loadingBar = this.add.graphics();
    this.loadingBar.fillStyle(0xffffff, 1);
    this.loadingBar.fillRect(
      this.cameras.main.width / 4 - 2,
      this.cameras.main.height / 2 - 18,
      this.cameras.main.width / 2 + 4,
      20
    );
    this.loadingBar.fillStyle(0x303000, 1);
    this.loadingBar.fillRect(
      this.cameras.main.width / 4 - 1,
      this.cameras.main.height / 2 - 17,
      this.cameras.main.width / 2 + 2,
      18
    );
    this.progressBar = this.add.graphics();
  }

  createAnimations(): void {
    const animationData: any = this.cache.json.get("animationJSON");
    for (let data of animationData.anims) {
      let frames;
      let framesArray: any[];
      if (data.frames.typeOfGeneration === "generateFrameNames") {
        frames = this.anims.generateFrameNames(data.frames.key, {
          prefix: data.frames.prefix || "",
          start: data.frames.start || 0,
          end: data.frames.end || 0,
          suffix: data.frames.suffix || "",
          zeroPad: data.frames.zeroPad || 0,
          frames: data.frames.frames || false,
        });
      } else if (data.frames.typeOfGeneration === "generateFrameNumbers") {
        frames = this.anims.generateFrameNumbers(data.frames.key, {
          start: data.frames.start || 0,
          end: data.frames.end || -1,
          first: data.frames.first || false,
          frames: data.frames.frames || false,
        });
      } else {
        for (let i of data.frames) {
          let frame = {
            key: i.key,
            frame: i.frame,
            duration: i.duration || 0,
            visible: i.visible,
          };
          framesArray.push(frame);
        }
      }

      this.anims.create({
        key: data.key,
        frames: frames || framesArray,
        defaultTextureKey: data.defaultTextureKey || null,
        frameRate: data.frameRate || 24,
        duration: data.duration,
        skipMissedFrames: data.skipMissedFrames || true,
        delay: data.delay || 0,
        repeat: data.repeat || 0,
        repeatDelay: data.repeatDelay || 0,
        yoyo: data.yoyo || false,
        showOnStart: data.showOnStart || false,
        hideOnComplete: data.hideOnComplete || false,
      });
    }
  }
}
