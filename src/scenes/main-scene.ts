import * as cfg from "../settings";
import { Player } from "../objects/player";
import { Person } from "../objects/person";
import { Enemy } from "../objects/enemy";
import { BaseScene } from './base-scene';
import * as Music from "../music";
import { PlatformerPhysics } from "../platformer-physics";

export class MainScene extends BaseScene {
  private toggleMusic: Phaser.Input.Keyboard.Key;
  private toggleMusicLocked: boolean;
  private lost: boolean;
  private timeRemaining: number;
  private player: Player;
  private platforms: Phaser.GameObjects.Group;
  private people: Phaser.GameObjects.Group;
  private enemies: Phaser.GameObjects.Group;
  private timer: Phaser.GameObjects.Text;
  private platformerPhysics:PlatformerPhysics;
  private followTarget:{x:number,y:number};

  constructor() {
    super({key: 'MainScene'});
    this.lost = false;
    this.player = null;
    this.platforms = null;
    this.people = null;
    this.enemies = null;
    this.timer = null;
    this.platformerPhysics = null;
    this.followTarget = null;
  }

  create(): void {
    this.lost = false;
    this.timeRemaining = cfg.TIME_LIMIT;

    this.toggleMusic = this.input.keyboard.addKey(
      Phaser.Input.Keyboard.KeyCodes.M
    );
    this.toggleMusicLocked = false;

    Music.playRandom();

    this.platformerPhysics = new PlatformerPhysics(0, cfg.GRAVITY);
    this.registry.set('platformerPhysics', this.platformerPhysics);

    this.addPlatforms();
    this.addNPCs();
    this.addPlayer();
    this.addTimer();
  }

  update(time: number, delta: number): void {
    super.update(time, delta);

    delta *= 0.001;

    this.player.update();

    this.platformerPhysics.beginFrame(delta);
    this.platformerPhysics.preventOverlap(this.player, this.platforms);
    this.platformerPhysics.preventOverlap(this.people, this.platforms, this.peoplePlatformCollision, this);
    this.platformerPhysics.preventOverlap(this.enemies, this.platforms);
    this.platformerPhysics.detectOverlap(this.player, this.people, this.playerPeopleOverlap, this);
    this.platformerPhysics.detectOverlap(this.player, this.enemies, this.playerEnemyOverlap, this);
    this.platformerPhysics.detectOverlap(this.people, this.enemies, this.peopleEnemyOverlap, this);
    this.platformerPhysics.endFrame(delta);
    this.player.lateUpdate();

    this.timeRemaining -= delta;
    this.updateTimer();

    // music controls
    if (this.toggleMusic.isUp) {
      this.toggleMusicLocked = false;
    }
    if (this.toggleMusic.isDown && !this.toggleMusicLocked) {
      this.toggleMusicLocked = true;
      Music.setVolume(cfg.MAX_VOLUME - Music.getVolume());
    }

    // camera
    this.followTarget && this.cameras.main.setScroll(
      Phaser.Math.Linear(this.cameras.main.scrollX, this.followTarget.x - 0.5 * this.cameras.main.width, cfg.CAMERA_TIGHTNESS),
      Phaser.Math.Linear(this.cameras.main.scrollY, this.followTarget.y - 0.5 * this.cameras.main.height, cfg.CAMERA_TIGHTNESS),
    );

    // time up
    if (this.timeRemaining <= 0) {
      this.switchToWinOrLoseScript();
    }
  }

  addPlatforms(): void {
    this.platforms = this.add.group();
    for (let rect of cfg.PLATFORMS) {
      const platform = this.add.rectangle(
        rect.x + 0.5 * rect.width,
        rect.y + 0.5 * rect.height,
        rect.width,
        rect.height,
        cfg.PLATFORM_COLOR
      );
      this.platforms.add(platform);
      
      this.platformerPhysics.add(platform);
      this.platformerPhysics.setFixed(platform, true);
      this.platformerPhysics.setAffectedByGravity(platform, false);
      this.platformerPhysics.setCollisionBounds(platform, -0.5 * rect.width, -0.5 * rect.height, rect.width, rect.height);
    }
  }

  addPlayer():void{
    this.player = new Player(this, 100, 225);
    this.player.addListener(cfg.EVENT_DEATH, () => this.lose(this.player));
    this.followTarget = this.player;
    this.add.existing(this.player);
  }

  addNPCs():void{
    this.people = this.add.group({ runChildUpdate: true });
    this.enemies = this.add.group({ runChildUpdate: true });

    const addPerson = (x:number,y:number)=> {
      const o = new Person(this, x, y);
      this.people.add(o);
      o.addListener(cfg.EVENT_DEATH, () => this.lose(o));
      this.add.existing(o);
    };
    const addGoombaLeft = (x:number,y:number)=>{
      const o = new Enemy(this, x, y)
      this.enemies.add(o);
      o.startWalkingLeft();
      this.add.existing(o);
    }
    const addGoombaRight = (x:number,y:number)=>{
      const o = new Enemy(this, x, y)
      this.enemies.add(o);
      o.startWalkingRight();
      this.add.existing(o);
    }
    // faller - early
    addPerson(325,-1200);
    
    // faller - late
    addPerson(520,-4800);
    
    // faller - later
    addPerson(560,-5100);
    
    // faller - latest
    addPerson(540,-5400);
    
    // rescue from goomba - near start
    addPerson(75, 220);
    
    // rescue from goomba - low right
    addPerson(700, 220);
    
    // rescue from goomba - high right
    addPerson(787, 140);
    
    // goomba - near start
    addGoombaLeft(255,230);
    
    // goomba - low right
    addGoombaRight(520,200);
    
    // goomba - high right
    addGoombaRight(600,-3300);
  }

  removeAll(): void {
    this.player.removeAllListeners(cfg.EVENT_DEATH);
    this.people.children.iterate((v) => v.removeAllListeners(cfg.EVENT_DEATH));

    for (let o of this.children.list) {
      o.destroy();
    }

    this.cameras.main.setScroll(0,0);

    this.player = null;
    this.platforms.clear();
    this.people.clear();
    this.enemies.clear();
  }

  lose(whoDied: Phaser.GameObjects.Sprite): void {
    if (!this.lost) {
      this.followTarget = whoDied;
    }

    this.lost = true;

    this.scheduleOnce(cfg.WAIT_FOR_ANIMATION_DELAY * 1000, () =>
      this.switchToWinOrLoseScript()
    );
  }

  switchToWinOrLoseScript(): void {
    this.removeAll();
    this.cleanupSchedule();
    if (this.lost) {
      this.scene.stop();
      this.scene.run('TryAgainScene');
    } else {
      this.scene.stop();
      this.scene.run('YouWinScene');
    }
  }

  addTimer():void{
    this.timer = this.add.text(0.5 * this.cameras.main.width, 14, '', {
      fontFamily: "'Courier New', monospace",
      fontSize: '18px',
      color: '#D5D5B3'
    });
    this.timer.setScrollFactor(0,0);
    this.timer.setOrigin(0.5,0.5);
  }

  updateTimer(): void {
    if (this.timeRemaining > 0) {
      const minutes = Math.floor(this.timeRemaining / 60.0);
      const minutesStr = minutes < 10 ? `0${minutes}` : `${minutes}`;
      const minutesRemainder = this.timeRemaining - minutes;
      const seconds = Math.floor(minutesRemainder);
      const secondsStr = seconds < 10 ? `0${seconds}` : `${seconds}`;
      const secondsRemainder = minutesRemainder - seconds;
      const hundredths: Number = Math.floor(secondsRemainder * 100);
      const hundredthsStr =
        hundredths < 10 ? `0${hundredths}` : `${hundredths}`;
      this.timer.setText(`${minutesStr}:${secondsStr}.${hundredthsStr}`);
    } else {
      this.timer.setText("00:00.00");
    }
  }

  playerPeopleOverlap(
    object1: undefined,
    object2: undefined
  ): void {
    const player = object1 as Player;
    const person = object2 as Person;
    if (!player.getIsDying() && !person.getIsDying()) {
      person.rescue();
    }
  }

  playerEnemyOverlap(
    object1: undefined,
    object2: undefined
  ): void {
    const p = object1 as Player;
    const e = object2 as Enemy;
    var pData = this.platformerPhysics.getPhysicsData(p);
    var eData = this.platformerPhysics.getPhysicsData(e);
    var pBottom = p.y + pData.y + pData.height;
    var eBottom = e.y + eData.y + eData.height;
    var above = pBottom < eBottom;
    if (!p.getIsDying() && !e.getIsStomped()) {
      if (above) {
        e.stomp();
        this.platformerPhysics.setVelocityY(p, -cfg.PLAYER_ENEMY_BOUNCE_SPEED);
      } else {
        p.startDeath();
      }
    }
  }

  peopleEnemyOverlap(
    object1: undefined,
    object2: undefined
  ): void {
    const p = object1 as Person;
    const e = object2 as Enemy;
    if (!p.getIsDying() && !e.getIsStomped()) {
      p.startDeath();
    }
  }

  peoplePlatformCollision(
    object1: undefined,
    object2: undefined
  ): void {
    const p = object1 as Person;
    if (!p.saved && (this.platformerPhysics.getVelocityY(p) >= cfg.MAX_FALL_SPEED)) {
      p.startDeath();
    }
  }
}
