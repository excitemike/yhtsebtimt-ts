
export class Cape extends Phaser.GameObjects.Sprite {
    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'sprites', 10);
        this.setOrigin(0.5, 0.5);
        this.anims.play('cape', true);
    }
}