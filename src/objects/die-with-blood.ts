import * as cfg from '../settings';
import { PlatformerPhysics } from "../platformer-physics";

export class DieWithBlood extends Phaser.GameObjects.Sprite {
    private isDying:boolean;

    constructor(scene: Phaser.Scene, x: number, y: number, texture: string | Phaser.Textures.Texture, frame?: string | number) {
        super(scene, x, y, texture, frame);
        this.isDying = false;
    }

    startDeath():void{
        this.emit(cfg.EVENT_DEATH);

        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.setVelocity(this,0,0);
        platformerPhysics.setAngularVelocity(this,cfg.DEATH_ROTATION);

        this.isDying = true;
        this.launchBlood(platformerPhysics);
        platformerPhysics.setFixed(this,true);
    }

    private launchBlood(platformerPhysics:PlatformerPhysics):void{
        for (let i=0;i<cfg.BLOOD_PARTICLES_PER_DEATH;++i){
            const s = this.scene.add.sprite(this.x, this.y, 'sprites', 9);
            platformerPhysics.setVelocity(s,
                Phaser.Math.Linear(-100,100,Math.random()),
                Phaser.Math.Linear(0,-300,Math.random())
            );
        }
    }

    getIsDying():boolean{return this.isDying;}
}