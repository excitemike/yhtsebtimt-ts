import { PlatformerPhysics } from '../platformer-physics';
import * as cfg from '../settings';

export class Enemy extends Phaser.GameObjects.Sprite {
    private isStomped:boolean;
    private direction:number;
    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'sprites', 10);

        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.add(this);
        platformerPhysics.setCollisionBounds(this,-5,0,10,10);
        platformerPhysics.setMaxSpeed(this, cfg.MAX_FALL_SPEED);

        this.setOrigin(0.5, 0.5);
        this.anims.play('enemy', true);
        this.isStomped = false;
        this.direction = 1;
    }
    update():void{
        if (!this.isStomped) {
            const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
            platformerPhysics.setVelocityX(this, this.direction * cfg.ENEMY_WALK_SPEED);
        }
    }
    startWalkingLeft():void{
        this.direction = -1;
        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.setVelocity(this, this.direction * cfg.ENEMY_WALK_SPEED, cfg.ENEMY_HOP_SPEED);
    }
    startWalkingRight():void{
        this.direction = 1;
        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.setVelocity(this, this.direction * cfg.ENEMY_WALK_SPEED, cfg.ENEMY_HOP_SPEED);
    }
    getIsStomped():boolean{return this.isStomped;}
    stomp():void{
        this.isStomped = true;
        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.setVelocity(this, 0, 0);
        platformerPhysics.disable(this);
        this.setScale(1,cfg.STOMPED_YSCALE);
        platformerPhysics.setCollisionBounds(this,-5,0 * cfg.STOMPED_YSCALE,10,10 * cfg.STOMPED_YSCALE);
        this.y += 10 * (1 - cfg.STOMPED_YSCALE);
    }
}