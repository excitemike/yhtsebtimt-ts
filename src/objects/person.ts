import { DieWithBlood } from './die-with-blood';
import * as cfg from '../settings';
import { PlatformerPhysics } from '../platformer-physics';

export class Person extends DieWithBlood {
    saved:boolean;
    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'sprites', 0);
        this.setOrigin(0.5, 0.5);
        this.anims.play('personPanic', true);

        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.add(this);
        platformerPhysics.setCollisionBounds(this,-6,-10,12,20);
        platformerPhysics.setMaxSpeed(this, cfg.MAX_FALL_SPEED);
        this.saved = false;
    }
    rescue():void{
        this.anims.play('personSaved', true);
        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.setVelocity(this,0,0);
        this.saved = true;
    }

}