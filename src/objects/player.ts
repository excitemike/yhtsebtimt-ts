import { Cape } from "./cape";
import { DieWithBlood } from './die-with-blood';
import * as cfg from '../settings';
import { PlatformerPhysics } from '../platformer-physics';

export class Player extends DieWithBlood {
    private cape:Cape;
    private left:Phaser.Input.Keyboard.Key;
    private right:Phaser.Input.Keyboard.Key;
    private up:Phaser.Input.Keyboard.Key;
    private prevUp:boolean;

    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'sprites', 3);

        this.cape = new Cape(scene, x, y);
        this.cape.setScale(2,2);
        scene.add.existing(this.cape);

        this.setOrigin(0.5, 0.5);
        this.anims.play('playerStand', true);

        this.left = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.right = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
        this.up = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        this.prevUp = false;

        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;
        platformerPhysics.add(this);
        platformerPhysics.setCollisionBounds(this,-10,-10,20,20);
        platformerPhysics.setMaxSpeed(this, cfg.MAX_FALL_SPEED);
    }

    destroy():void {
        if (this.cape != null) {
            this.cape.destroy();
            this.cape = null;
        }
        super.destroy();
    }

    update(): void {
        super.update();

        const platformerPhysics = this.scene.registry.get('platformerPhysics') as PlatformerPhysics;

        // walking
        let dx = 0;
        let dy = platformerPhysics.getVelocityY(this);
        if (this.left.isDown) {
            dx -= cfg.PLAYER_WALK_SPEED;
        }
        if (this.right.isDown) {
            dx += cfg.PLAYER_WALK_SPEED;
        }

        // jumping
        const onGround = platformerPhysics.getCollidedBottom(this);
        if (this.up.isDown && !this.prevUp && onGround) {
            dy -= cfg.PLAYER_JUMP_SPEED;
        }
        if (!this.up.isDown && this.prevUp && !onGround && (dy < 0)) {
            dy = 0;
        }
        this.prevUp = this.up.isDown;

        // switch to the correct animation
        platformerPhysics.setVelocity(this, dx, dy);
        if (dx > 0) {
            this.flipX = false;
        }
        if (dx < 0) {
            this.flipX = true;
        }
        if (onGround) {
            if (dx === 0.0) {
                this.anims.play('playerStand', true);
            } else {
                this.anims.play('playerRun', true);
            }
        } else {
            this.anims.play('playerJump', true);
        }
    }

    lateUpdate():void{
        this.cape.x = this.x;
        this.cape.y = this.y;
        this.cape.flipX = this.flipX;
    }
}