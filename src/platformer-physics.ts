type PhysicsData = {
  /// offset from gameobject origin to left edge of collision box
  x: number;
  /// offset from gameobject origin to top edge of collision box
  y: number;
  /// width of collision box
  width: number;
  /// height of collision box
  height: number;
  /// movement rate
  dx: number;
  /// movement rate
  dy: number;
  /// rotation rate
  dr: number;
  /// amount of movement to take into account
  xmove: number;
  /// amount of movement to take into account
  ymove: number;
  /// remember sides on which collisions may have happened
  collidedTop: boolean;
  /// remember sides on which collisions may have happened
  collidedRight: boolean;
  /// remember sides on which collisions may have happened
  collidedBottom: boolean;
  /// remember sides on which collisions may have happened
  collidedLeft: boolean;
  /// is the object's motion affected by collisions?
  fixed: boolean;
  /// relative mass of the object. when objects collide, the lighter one will experience the larger change in velocity
  mass:number,
  /// whether to run physics on this object
  enabled:boolean,//TODO: support this
  /// whether to apply gravity to this object
  affectedByGravity:boolean,
  /// limit velocity
  maxSpeed:number
};
function makeDefaultData(): PhysicsData {
  return {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
    dx: 0,
    dy: 0,
    dr: 0,
    xmove: 0,
    ymove: 0,
    collidedTop: false,
    collidedRight: false,
    collidedBottom: false,
    collidedLeft: false,
    fixed: false,
    mass: 1,
    enabled: true,
    affectedByGravity: true,
    maxSpeed: Number.POSITIVE_INFINITY
  };
}
export type GameObject =
  | Phaser.GameObjects.Shape
  | Phaser.GameObjects.Image
  | Phaser.GameObjects.Sprite;
export type CollisionCallback = (a: GameObject, b: GameObject) => void;

export class PlatformerPhysics {
  private map: Map<GameObject, PhysicsData>;
  private gravityX:number;
  private gravityY:number;

  constructor(gravityX:number, gravityY:number) {
    this.map = new Map();
    this.gravityX = gravityX;
    this.gravityY = gravityY;
  }

  add(o: GameObject): void {
    this.getOrAdd(o);
  }

  clear(): void {
    this.map.clear();
  }

  private getOrAdd(o:GameObject):PhysicsData {
    let data = this.map.get(o);
    if (data === undefined) {
      o.addListener(Phaser.GameObjects.Events.DESTROY, this.objectDestroyed, this);
      data = makeDefaultData();
      this.map.set(o, data);
    }
    return data;
  }

  setVelocity(o:GameObject, dx:number, dy:number):void{
    let data = this.getOrAdd(o);
    data.dx = dx;
    data.dy = dy;
  }

  setAngularVelocity(o:GameObject, dr:number):void{
    let data = this.getOrAdd(o);
    data.dr = dr;
  }

  setCollisionBounds(o:GameObject, x:number, y:number, width:number, height:number):void {
    let data = this.getOrAdd(o);
    data.x = x;
    data.y = y;
    data.width = width;
    data.height = height;
  }

  setVelocityX(o:GameObject, dx:number):void{
    let data = this.getOrAdd(o);
    data.dx = dx;
  }

  setVelocityY(o:GameObject, dy:number):void{
    let data = this.getOrAdd(o);
    data.dy = dy;
  }

  setMaxSpeed(o:GameObject, maxSpeed:number):void{
    let data = this.getOrAdd(o);
    data.maxSpeed = maxSpeed;
  }

  disable(o:GameObject):void{
    let data = this.getOrAdd(o);
    data.enabled = false;
  }
  
  setFixed(o:GameObject,fixed:boolean):void{
    let data = this.getOrAdd(o);
    data.fixed = fixed;
  }
  
  setAffectedByGravity(o:GameObject,affectedByGravity:boolean):void{
    let data = this.getOrAdd(o);
    data.affectedByGravity = affectedByGravity;
  }

  getCollidedBottom(o:GameObject):boolean {
    const data = this.map.get(o);
    return (data===undefined) ? false : data.collidedBottom;
  }

  getVelocityY(o:GameObject):number {
    const data = this.map.get(o);
    return (data===undefined) ? 0 : data.dy;
  }

  getPhysicsData(o:GameObject):PhysicsData {
    return this.map.get(o);
  }

  stop(o:GameObject):void {
    const data = this.map.get(o);
    if (data!==undefined) {
      data.dx = 0;
      data.dy = 0;
      data.xmove = 0;
      data.ymove = 0;
    }
  }

  beginFrame(deltaTime: number): void {
    const deltaVx = this.gravityX * deltaTime;
    const deltaVy = this.gravityY * deltaTime;
    this.map.forEach((v, k, m) => {
      if (v.affectedByGravity) {
        var newDx = v.dx + deltaVx;
        var newDy = v.dy + deltaVy;
        var uncorrectedSpeed = Math.sqrt(newDx*newDx + newDy*newDy);
        if (uncorrectedSpeed > v.maxSpeed) {
          newDx *= v.maxSpeed / uncorrectedSpeed;
          newDy *= v.maxSpeed / uncorrectedSpeed;
        }
        var avgDx = 0.5 * (v.dx + newDx);
        var avgDy = 0.5 * (v.dy + newDy);
        v.xmove = avgDx * deltaTime;
        v.ymove = avgDy * deltaTime;
        v.dx += deltaVx;
        v.dy = newDy;
        /*v.xmove = (v.dx + 0.5 * deltaVx) * deltaTime;
        v.ymove = (v.dy + 0.5 * deltaVy) * deltaTime;
        v.dx += deltaVx;
        v.dy += deltaVy;*/
      } else {
        v.xmove = v.dx * deltaTime;
        v.ymove = v.dy * deltaTime;
      }
      v.collidedTop = false;
      v.collidedRight = false;
      v.collidedBottom = false;
      v.collidedLeft = false;
    });
  }

  detectOverlap(
    a: GameObject | GameObject[] | Phaser.GameObjects.Group,
    b: GameObject | GameObject[] | Phaser.GameObjects.Group,
    contactCb?: CollisionCallback,
    callbackContext?: any
  ): void {
    if (a === b) {
      return;
    } else if (
      a instanceof Phaser.GameObjects.Shape ||
      a instanceof Phaser.GameObjects.Image ||
      a instanceof Phaser.GameObjects.Sprite
    ) {
      this.detectOverlap_internal1(a, b, contactCb, callbackContext);
    } else if (a instanceof Phaser.GameObjects.Group) {
      for (let child of a.getChildren()) {
        this.detectOverlap_internal1(child as GameObject, b, contactCb, callbackContext);
      }
    } else {
      for (let child of a) {
        this.detectOverlap_internal1(child, b, contactCb, callbackContext);
      }
    }
  }

  preventOverlap(
    a: GameObject | GameObject[] | Phaser.GameObjects.Group,
    b: GameObject | GameObject[] | Phaser.GameObjects.Group,
    contactCbBefore?: CollisionCallback,
    callbackContext?: any
  ): void {
    if (a === b) {
      return;
    } else if (
      a instanceof Phaser.GameObjects.Shape ||
      a instanceof Phaser.GameObjects.Image ||
      a instanceof Phaser.GameObjects.Sprite
    ) {
      this.preventOverlap_internal1(a, b, contactCbBefore, callbackContext);
    } else if (a instanceof Phaser.GameObjects.Group) {
      for (let child of a.getChildren()) {
        this.preventOverlap_internal1(child as GameObject, b, contactCbBefore, callbackContext);
      }
    } else {
      for (let child of a) {
        this.preventOverlap_internal1(child, b, contactCbBefore, callbackContext);
      }
    }
  }

  private preventOverlap_internal1(
    a: GameObject,
    b: GameObject | GameObject[] | Phaser.GameObjects.Group,
    contactCbBefore?: CollisionCallback,
    callbackContext?: any
  ):void {
    if (
      b instanceof Phaser.GameObjects.Shape ||
      b instanceof Phaser.GameObjects.Image ||
      b instanceof Phaser.GameObjects.Sprite
    ) {
      this.preventOverlap_internal2(a, b, contactCbBefore, callbackContext);
    } else if (b instanceof Phaser.GameObjects.Group) {
      for (let child of b.getChildren()) {
        this.preventOverlap_internal2(a, child as GameObject, contactCbBefore, callbackContext);
      }
    } else {
      for (let child of b) {
        this.preventOverlap_internal2(a, child, contactCbBefore, callbackContext);
      }
    }
  }

  private detectOverlap_internal1(
    a: GameObject,
    b: GameObject | GameObject[] | Phaser.GameObjects.Group,
    contactCb?: CollisionCallback,
    callbackContext?: any
  ):void {
    if (
      b instanceof Phaser.GameObjects.Shape ||
      b instanceof Phaser.GameObjects.Image ||
      b instanceof Phaser.GameObjects.Sprite
    ) {
      this.detectOverlap_internal2(a, b, contactCb, callbackContext);
    } else if (b instanceof Phaser.GameObjects.Group) {
      for (let child of b.getChildren()) {
        this.detectOverlap_internal2(a, child as GameObject, contactCb, callbackContext);
      }
    } else {
      for (let child of b) {
        this.detectOverlap_internal2(a, child, contactCb, callbackContext);
      }
    }
  }

  private preventOverlap_internal2(
    a: GameObject,
    b: GameObject,
    contactCbBefore?: CollisionCallback,
    callbackContext?: any
  ):void {
    const aData = this.map.get(a);
    const bData = this.map.get(b);

    if (aData.fixed && bData.fixed) {
        return;
    }

    const aTop = a.y + aData.y;
    const aBottom = aTop + aData.height;
    const aLeft = a.x + aData.x;
    const aRight = aLeft + aData.width;
    const bTop = b.y + bData.y;
    const bBottom = bTop + bData.height;
    const bLeft = b.x + bData.x;
    const bRight = bLeft + bData.width;

    // minkowski difference
    var diffTop = aTop - bBottom;
    var diffRight = aRight - bLeft;
    var diffBottom = aBottom - bTop;
    var diffLeft = aLeft - bRight;

    // if already overlapping, we must first separate
    if ((diffLeft < 0) && (diffTop < 0) && (diffRight > 0) && (diffBottom > 0)) {
      var minCorrection = Math.min(-diffLeft, -diffTop, diffRight, diffBottom);
      if (-diffTop === minCorrection) {
        // move down
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_top(a,aData,b,bData,0);
      } else if (diffBottom === minCorrection) {
        // move up
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_bottom(a,aData,b,bData,0);
      } else if (-diffLeft === minCorrection) {
        // move right
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_left(a,aData,b,bData,0);
      } else {
        // move left
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_right(a,aData,b,bData,0);
      }
      return;
    }

    // need to check motion
    const movementX = aData.xmove - bData.xmove;
    const movementY = aData.ymove - bData.ymove;
    if ((diffTop >= 0) && (movementY<0)) {
      const t = diffTop / -movementY;
      if ((t <= 1.0) &&
          (diffLeft + t * movementX < 0) &&
          (diffRight + t * movementX > 0)
      ) {
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_top(a,aData,b,bData,t);
        return;
      }
    }
    if ((diffBottom <= 0) && (movementY>0)) {
      const t = -diffBottom / movementY;
      if ((t <= 1.0) &&
          (diffLeft + t * movementX < 0) &&
          (diffRight + t * movementX > 0)
      ) {
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_bottom(a,aData,b,bData,t);
        return;
      }
    }
    if ((diffLeft >= 0) && (movementX<0)) {
      const t = diffLeft / -movementX;
      if ((t <= 1.0) &&
          (diffTop + t * movementY < 0) &&
          (diffBottom + t * movementY > 0)
      ) {
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_left(a,aData,b,bData,t);
        return;
      }
    }
    if ((diffRight <= 0) && (movementX>0)) {
      const t = -diffRight / movementX;
      if ((t <= 1.0) &&
        (diffTop + t * movementY < 0) &&
        (diffBottom + t * movementY > 0)
      ) {
        contactCbBefore && contactCbBefore.call(callbackContext, a, b);
        preventOverlap_right(a,aData,b,bData,t);
        return;
      }
    }
  }

  detectOverlap_internal2(
    a: GameObject,
    b: GameObject,
    contactCb?: CollisionCallback,
    callbackContext?: any
  ):void {
    const aData = this.map.get(a);
    const bData = this.map.get(b);

    if (aData.fixed && bData.fixed) {
        return;
    }

    const aTop = a.y + aData.y;
    const aBottom = aTop + aData.height;
    const aLeft = a.x + aData.x;
    const aRight = aLeft + aData.width;
    const bTop = b.y + bData.y;
    const bBottom = bTop + bData.height;
    const bLeft = b.x + bData.x;
    const bRight = bLeft + bData.width;

    // minkowski difference
    var diffTop = aTop - bBottom;
    var diffRight = aRight - bLeft;
    var diffBottom = aBottom - bTop;
    var diffLeft = aLeft - bRight;

    // if already overlapping, we must first separate
    if ((diffLeft < 0) && (diffTop < 0) && (diffRight > 0) && (diffBottom > 0)) {
      contactCb && contactCb.call(callbackContext, a, b);
      return;
    }

    // need to check motion
    const movementX = aData.xmove - bData.xmove;
    const movementY = aData.ymove - bData.ymove;
    if ((diffTop >= 0) && (movementY<0)) {
      const t = diffTop / -movementY;
      if ((t <= 1.0) &&
          (diffLeft + t * movementX < 0) &&
          (diffRight + t * movementX > 0)
      ) {
        contactCb && contactCb.call(callbackContext, a, b);
        return;
      }
    }
    if ((diffBottom <= 0) && (movementY>0)) {
      const t = -diffBottom / movementY;
      if ((t <= 1.0) &&
          (diffLeft + t * movementX < 0) &&
          (diffRight + t * movementX > 0)
      ) {
        contactCb && contactCb.call(callbackContext, a, b);
        return;
      }
    }
    if ((diffLeft >= 0) && (movementX<0)) {
      const t = diffLeft / -movementX;
      if ((t <= 1.0) &&
          (diffTop + t * movementY < 0) &&
          (diffBottom + t * movementY > 0)
      ) {
        contactCb && contactCb.call(callbackContext, a, b);
        return;
      }
    }
    if ((diffRight <= 0) && (movementX>0)) {
      const t = -diffRight / movementX;
      if ((t <= 1.0) &&
        (diffTop + t * movementY < 0) &&
        (diffBottom + t * movementY > 0)
      ) {
        contactCb && contactCb.call(callbackContext, a, b);
        return;
      }
    }
  }

  endFrame(deltaTime: number): void {
    this.map.forEach((v, k, m) => {
      k.setAngle(k.angle + v.dr * deltaTime);
      k.x += v.xmove;
      k.y += v.ymove;
    });
  }
  
  private objectDestroyed(o:GameObject):void{
    o.removeListener(Phaser.GameObjects.Events.DESTROY, this.objectDestroyed, this);
    this.map.delete(o);
  }
}

/// react to top of a hitting bottom of b
function preventOverlap_top(
    a: GameObject,
    aData: PhysicsData,
    b: GameObject,
    bData: PhysicsData,
    t: number
): void {
    if (aData.fixed) {
        if (bData.fixed) {
            return;
        } else {
            bData.ymove = a.y + aData.y + aData.ymove - b.y - bData.y - bData.height;
            if (aData.dy < bData.dy) {
                bData.dy = aData.dy;
            }
        }
    } else if (bData.fixed) {
        aData.ymove = b.y + bData.y + bData.height + bData.ymove - a.y - aData.y;
        if (aData.dy < bData.dy) {
            aData.dy = bData.dy;
        }
    } else {
        aData.ymove *= t;
        bData.ymove *= t;
        if (aData.dy < bData.dy) {
            const m1 = aData.mass;
            const m2 = bData.mass;
            const v1 = aData.dy;
            const v2 = bData.dy;
            aData.dy = (m1*v1 - m2*v1 + 2*m2*v2)/(m1+m2);
            bData.dy = (m2*v2 - m1*v2 + 2*m1*v1)/(m1+m2);
        }
    }
    aData.collidedTop = true;
    bData.collidedBottom = true;
}

/// react to bottom of a hitting top of b
function preventOverlap_bottom(
    a: GameObject,
    aData: PhysicsData,
    b: GameObject,
    bData: PhysicsData,
    t: number
): void {
    if (aData.fixed) {
        if (bData.fixed) {
            return;
        } else {
            bData.ymove = a.y + aData.y + aData.height + aData.ymove - b.y - bData.y;
            if (aData.dy > bData.dy) {
                bData.dy = aData.dy;
            }
        }
    } else if (bData.fixed) {
        aData.ymove = b.y + bData.y + bData.ymove - a.y - aData.y - aData.height;
        if (aData.dy > bData.dy) {
            aData.dy = bData.dy;
        }
    } else {
        aData.ymove *= t;
        bData.ymove *= t;
        if (aData.dy > bData.dy) {
            const m1 = aData.mass;
            const m2 = bData.mass;
            const v1 = aData.dy;
            const v2 = bData.dy;
            aData.dy = (m1*v1 - m2*v1 + 2*m2*v2)/(m1+m2);
            bData.dy = (m2*v2 - m1*v2 + 2*m1*v1)/(m1+m2);
        }
    }
    aData.collidedBottom = true;
    bData.collidedTop = true;
}

/// react to left side of a hitting right side of b
function preventOverlap_left(
    a: GameObject,
    aData: PhysicsData,
    b: GameObject,
    bData: PhysicsData,
    t: number
): void {
  if (aData.fixed) {
      if (bData.fixed) {
          return;
      } else {
          bData.xmove = a.x + aData.x + aData.xmove - b.x - bData.x - bData.width;
          if (aData.dx < bData.dx) {
              bData.dx = aData.dx;
          }
      }
  } else if (bData.fixed) {
      aData.xmove = b.x + bData.x + bData.width + bData.xmove - a.x - aData.x;
      if (aData.dx < bData.dx) {
          aData.dx = bData.dx;
      }
  } else {
      aData.xmove *= t;
      bData.xmove *= t;
      if (aData.dx < bData.dx) {
          const m1 = aData.mass;
          const m2 = bData.mass;
          const v1 = aData.dx;
          const v2 = bData.dx;
          aData.dx = (m1*v1 - m2*v1 + 2*m2*v2)/(m1+m2);
          bData.dx = (m2*v2 - m1*v2 + 2*m1*v1)/(m1+m2);
      }
  }
  aData.collidedLeft = true;
  bData.collidedRight = true;
}

/// react to right side of a hitting left side of b
function preventOverlap_right(
    a: GameObject,
    aData: PhysicsData,
    b: GameObject,
    bData: PhysicsData,
    t: number
): void {
    if (aData.fixed) {
        if (bData.fixed) {
            return;
        } else {
            bData.xmove = a.x + aData.x + aData.width + aData.xmove - b.x - bData.x;
            if (aData.dx > bData.dx) {
                bData.dx = aData.dx;
            }
        }
    } else if (bData.fixed) {
        aData.xmove = b.x + bData.x + bData.xmove - a.x - aData.x - aData.width;
        if (aData.dx > bData.dx) {
            aData.dx = bData.dx;
        }
    } else {
        aData.xmove *= t;
        bData.xmove *= t;
        if (aData.dx > bData.dx) {
            const m1 = aData.mass;
            const m2 = bData.mass;
            const v1 = aData.dx;
            const v2 = bData.dx;
            aData.dx = (m1*v1 - m2*v1 + 2*m2*v2)/(m1+m2);
            bData.dx = (m2*v2 - m1*v2 + 2*m1*v1)/(m1+m2);
        }
    }
    aData.collidedRight = true;
    bData.collidedLeft = true;
}

